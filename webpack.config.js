const HtmlWebpackPlugin = require('html-webpack-plugin');

const CommonConfig = require('./webpack.common.config');

module.exports = [
  Object.assign(
    {
      target: 'electron-main',
      entry: {main: './electron/index.ts'}
    },
    CommonConfig
  ),
  Object.assign(
    {
      target: 'electron-renderer',
      entry: {gui: './react/index.tsx'},
      plugins: [new HtmlWebpackPlugin()]
    },
    CommonConfig
  )
];
